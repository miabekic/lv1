﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zabiljeska
{
    class Note
    {
        private string text;
        private string autor;
        private int importanceDegree;
        public string getText()
        {
            return text;
        }
        public string getAutor()
        {
            return autor;
        }
        public int getImportanceDegree()
        {
            return importanceDegree;
        }
        public void setText(string txt)
        {
            text = txt;
        }
        public void setImportanceDegree(int degree)
        {
            importanceDegree = degree;
        }

        public string Text
        {
            get { return text; }
            set { text = value; }
        }
        public int ImportanceDegree
        {
            get { return importanceDegree; }
            set { importanceDegree = value; }
        }
        public string Autor
        {
            get { return autor; }
        }
        public Note()
        {
            text = "";
            autor = "";
            importanceDegree = 0;
        }
        public Note(string Autor)
        {
            text = "";
            autor = Autor;
            importanceDegree = 0;
        }
        public Note(string txt, string Autor, int ImportanceDegree)
        {
            text = txt;
            autor = Autor;
            importanceDegree = ImportanceDegree;
        }
        public override string ToString()
        {
            return text + ", " + autor + ", " + importanceDegree + "\n";
        }
    }
}
