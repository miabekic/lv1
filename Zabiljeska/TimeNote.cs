﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zabiljeska
{
    class TimeNote : Note
    {
        public DateTime time { get; private set; }
        public TimeNote(string text, string autor, int importanceDegree) : base(text, autor, importanceDegree)
        {
            time = DateTime.Now;
        }
        public override string ToString()
        {
            return Text + ", " + Autor + ", " + ImportanceDegree + "\n" + time + "\n";
        }
    }
}

