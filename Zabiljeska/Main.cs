﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks; 


namespace Zabiljeska
{

    class Program
    {
            static void Main(string[] args)
            {
            Note Note1 = new Note();
            Note Note2 = new Note("Mia");
            Note Note3 = new Note("Danas nam je divan dan", "Sanja", 1);
            Console.WriteLine(Note1.getText());
            Console.WriteLine(Note1.Autor + "\n");
            Console.WriteLine(Note2.Text);
            Console.WriteLine(Note2.Autor + "\n");
            Console.WriteLine(Note3.getText());
            Console.WriteLine(Note3.getAutor() + "\n");
            List<TimeNote> times = new List<TimeNote>()
            {
                new TimeNote("Kupi slatkise!", "Sestra", 2),
                new TimeNote("Sviranje u 16:30h", "Profesor", 2),
                new TimeNote("Gledaj film", "Nora", 0),
                new TimeNote("Dobar dan", "Andreja", 1)
            };

            ToDo notesForDo = new ToDo(times);
            Console.WriteLine(notesForDo.ToString());
            Console.WriteLine(notesForDo.GetNote(2)+"\n");
            notesForDo.RemoveNote();
            Console.WriteLine(notesForDo.ToString());
            }
        }
    }

