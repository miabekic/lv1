﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zabiljeska
{
    class ToDo
    {
        private List<TimeNote> toDoNotes;
        public ToDo(List<TimeNote> notes)
        {
            toDoNotes = notes;
        }
        public void AddNote(TimeNote newNote)

        {
            toDoNotes.Add(newNote);
        }
        public void RemoveNote()
        {
            for (int i = 0; i < toDoNotes.Count; i++)
            {
                if (toDoNotes[i].ImportanceDegree == 2)
                {
                    toDoNotes.RemoveAt(i);
                    i--;
                }
            }

        }
        public TimeNote GetNote(int n)
        {
            n -= 1;
            return toDoNotes[n];
        }
        public override string ToString()
        {
            string allNotes = "";
            for (int i = 0; i < toDoNotes.Count; i++)
            {
                allNotes += toDoNotes[i].ToString() + "\n";
            }
            return allNotes;
        }

    }
}

